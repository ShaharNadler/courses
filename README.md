## Full list of courses
~~~
Matriculation Certificate | 06/2020 

DevOps Developer Expert, Sela | 03/2023-08/23

System-Administrator, IDF | 03/2021

DevOps, IDF | 12/2021

Postgres, IDF |12/2022

Building Data Lakes on AWS | 12/2022

Security engineering on AWS |12/2022

Data Warehousing on AWS | 11/2022

AWS Technical Essentials | 11/2022

Running Containers on Amazon EKS | 10/2022

Engine Application Development With Cloud Run | 12/2022

Google Cloud Fundamentals (Big Data & ML) | 11/2022

Getting Started with Google Kubernetes | 11/2022

Architecting With Google Cloud: Design and Process | 09/2022

Developing Application with Google Cloud | 09/2022
~~~
